<?php

namespace Drupal\commerce_stripe_bancontact\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\entity\BundleFieldDefinition;
use Drupal\commerce_payment\CreditCard as CreditCardHelper;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;

/**
 * Provides the bancontact payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "bancontact",
 *   label = @Translation("Bancontact"),
 * )
 */
class Bancontact extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    // TODO: Implement buildLabel() method.
  }


}
